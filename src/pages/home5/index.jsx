import React from "react";
import Team2 from "../../components/Team2";
import Video from "../../components/Video";
// import MainLayout from "../../layouts/main";
import MainLayout from "../../layouts/main-skin";
import Blogs4 from "../../components/Blogs4";
// import Intro4 from "../../components/Intro4";
import Intro5 from "../../components/Intro5";
import AboutUs6 from "../../components/About-Us6";
import Services3 from "../../components/Services3";
import Portfolio2 from "../../components/Portfolio2";
import Testimonials1 from "../../components/Testimonials1";
import Brands from "../../components/Brands";

const Home5 = () => {
  React.useEffect(() => {
    document.querySelector("body").classList.add("index3");
  }, []);
  return (
    <MainLayout>
      <Intro5 />
      <Brands />
      <Video />
      <Services3 bigTitle grid />
      <Portfolio2 />
      <AboutUs6 />
      <Team2 />
      <Testimonials1 bigTitle />
      <Blogs4 />
    </MainLayout>
  );
};

export default Home5;
