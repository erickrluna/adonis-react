/* eslint-disable @next/next/no-img-element */
import React from "react";

const Brands = () => {
  return (
    <div className="brands bg-gray">
      <div className="container box-top wow fadeInUp" data-wow-delay=".3s">
        <div className="row">
          <div className="col-lg col-md-3 col-sm-6">
            <div className="item mb-5 mt-5">
              <div className="img">
                <img src="/assets/img/clients/brands/07.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-lg col-md-3 col-sm-6">
            <div className="item mb-5 mt-5">
              <div className="img">
                <img src="/assets/img/clients/brands/08.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-lg col-md-3 col-sm-6">
            <div className="item mb-5 mt-5">
              <div className="img">
                <img src="/assets/img/clients/brands/09.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-lg col-md-3 col-sm-6">
            <div className="item mb-5 mt-5">
              <div className="img">
                <img src="/assets/img/clients/brands/10.png" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Brands;
