import React from "react";
import Split from "../Split";
import Link from "next/link";

const Intro5 = () => {
  return (
    <header
      className="header bg-img valign"
      // style={{ backgroundImage: "url(/assets/img/slid/x1.jpg)" }}
      style={{ backgroundImage: "url(/assets/img/slid/x1-1.jpg)" }}
      data-overlay-dark="5" // 0 - 9
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-10 offset-lg-1">
            <div className="caption hmone">
              <Split>
                {/* <h1 data-splitting className="words chars splitting">
                  <Link href="#">Elegant &amp; Unique Design</Link>
                  <div className="bord"></div>
                </h1> */}
                <h2 data-splitting className="words chars splitting">
                  <Link href="#">Welcome to Adonis Tourism Website</Link>
                  <div className="bord"></div>
                </h2>
              </Split>
              <h5 className="mt-10">
                You Travel, We Care. <br /> Dubai, United Arab Emirates.
              </h5>
              <Link href="/project-details">
                <a className="btn-curve btn-bord btn-lit mt-30">
                  <span>Discover More</span>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Intro5;
