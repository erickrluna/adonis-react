/* eslint-disable @next/next/no-img-element */
import React from "react";
import Link from "next/link";
import appData from "../../data/app.json";
import getSiblings from "../../common/getSiblings";

const Navbar = ({ navbarRef, logoRef, logoClass }) => {
  const handleDropdown = (e) => {
    getSiblings(e.target.parentElement)
      .filter((item) => item.classList.contains("show"))
      .map((item) => {
        item.classList.remove("show");
        if (item.childNodes[0]) {
          item.childNodes[0].setAttribute("aria-expanded", false);
        }
        if (item.childNodes[1]) {
          item.childNodes[1].classList.remove("show");
        }
      });
    e.target.parentElement.classList.toggle("show");
    e.target.setAttribute("aria-expanded", true);
    e.target.parentElement.childNodes[1].classList.toggle("show");
  };

  const handleMobileDropdown = (e) => {
    document
      .getElementById("navbarSupportedContent")
      .classList.toggle("show-with-trans");
  };

  return (
    <>
      <nav className="navbar change navbar-expand-lg" ref={navbarRef}>
        <div className="container">
          <Link href="/demos">
            <a className={`logo ${logoClass && logoClass}`}>
              <img src={appData.lightLogo} alt="logo" ref={logoRef} />
            </a>
          </Link>

          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            onClick={handleMobileDropdown}
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="icon-bar">
              <i className="fas fa-bars"></i>
            </span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              {/* <li className="nav-item dropdown" onClick={handleDropdown}>
                <span
                  className="nav-link dropdown-toggle"
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Home
                </span>
                <div className="dropdown-menu">
                  <Link href="/home7">
                    <a className="dropdown-item">Home Default</a>
                  </Link>
                  <Link href="/home6">
                    <a className="dropdown-item">Interior Agency</a>
                  </Link>
                  <Link href="/home5">
                    <a className="dropdown-item">Creative Arch</a>
                  </Link>
                  <Link href="/home4">
                    <a className="dropdown-item">Architecture Studio</a>
                  </Link>
                  <Link href="/home1">
                    <a className="dropdown-item">Home Light</a>
                  </Link>
                  <Link href="/home2">
                    <a className="dropdown-item">Interior Light</a>
                  </Link>
                  <Link href="/home3">
                    <a className="dropdown-item">Architecture Light</a>
                  </Link>
                </div>
              </li> */}
              <li className="nav-item">
                <Link href="/">
                  <a className="nav-link">Home</a>
                </Link>
              </li>
              <li className="nav-item dropdown" onClick={handleDropdown}>
                <span
                  className="nav-link dropdown-toggle"
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Destinations
                </span>
                <div className="dropdown-menu">
                  <Link href="/">
                    <a className="dropdown-item">United Arab Emirates</a>
                  </Link>
                  <Link href="#">
                    <a className="dropdown-item">Sultanate of Oman</a>
                  </Link>
                </div>
              </li>
              <li className="nav-item dropdown" onClick={handleDropdown}>
                <span
                  className="nav-link dropdown-toggle"
                  data-toggle="dropdown"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Services
                </span>
                <div className="dropdown-menu">
                  <Link href="/home7">
                    <a className="dropdown-item">Meetings & Events</a>
                  </Link>
                  <Link href="/home6">
                    <a className="dropdown-item">Hotel Bookings</a>
                  </Link>
                  <Link href="/home6">
                    <a className="dropdown-item">Group Bookings</a>
                  </Link>
                  <Link href="/home6">
                    <a className="dropdown-item">Transfers</a>
                  </Link>
                  <Link href="/home6">
                    <a className="dropdown-item">Visa Services</a>
                  </Link>
                </div>
              </li>
              <li className="nav-item">
                <Link href="/about">
                  <a className="nav-link">Tour Packages</a>
                </Link>
              </li>
              <li className="nav-item dropdown" onClick={handleDropdown}>
                <span
                  className="nav-link dropdown-toggle"
                  data-toggle="dropdown"
                  href="#"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Things To Do
                </span>
                <div className="dropdown-menu">
                  <Link href="/work1">
                    <a className="dropdown-item">Dubai Activities</a>
                  </Link>
                  <Link href="/work2">
                    <a className="dropdown-item">
                      Abu Dhabi & Al Ain Activities
                    </a>
                  </Link>
                  <Link href="/work3">
                    <a className="dropdown-item">Other Emirates Activities</a>
                  </Link>
                </div>
              </li>
              <li className="nav-item dropdown" onClick={handleDropdown}>
                <span
                  className="nav-link dropdown-toggle"
                  data-toggle="dropdown"
                  href="#"
                  role="button"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Must See!
                </span>
                <div className="dropdown-menu">
                  <Link href="/blogs">
                    <a className="dropdown-item">Top Attractions UAE</a>
                  </Link>
                  <Link href="/blog-details">
                    <a className="dropdown-item">Top Attractions Oman</a>
                  </Link>
                </div>
              </li>
              <li className="nav-item">
                <Link href="/contact">
                  <a className="nav-link">Contact</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
